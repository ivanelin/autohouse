<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        return view('pages.index'); 
    }

    public function  car_in() {
        return view('pages.car_in');
    }

    public function buy() {
        
        return view('pages.buy');
    }

    public function contacts() {
        
        return view('pages.contacts');
    }
}
