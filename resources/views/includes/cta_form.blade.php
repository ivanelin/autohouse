<section class="form_secion">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-10">
                <div class="form-bg">
                    <div class="row">
                        <div class="col-8 align-items-center">
                            <div class="form-text">
                                <h4>Не гайте часу на самостіні пошуки</h4>
                                <p>Залиште нам свої контакти і ми відразу розкажемо Вам все, що Вас цікавить</p>
                            </div>
                            <form class="cta_form">
                                <input type="text" class="form-control" id="nameInput" placeholder="Ім`я">

                                <input type="text" class="form-control" id="phone_nailInput"
                                       placeholder="Номер телефону або пошта">

                                <textarea name="Comment" id="comment_area" placeholder="Коментар"></textarea>

                                <button type="submit" class="form-button">Надіслати</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>