<div class="container">
        <nav class="navbar navbar-inverse navbar-expand-lg">
            <a href="#" class="navbar-brand"><img src="{{ asset('img/LOGO.png') }}" alt="logo"></a>

            <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target" >
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="collapse_target">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item menu_item active"><a href="/" class="nav-link ">Головна</a></li>
                    <li class="nav-item menu_item"><a href="/car_in" class="nav-link">Авто в ноявності</a></li>
                    <li class="nav-item menu_item"><a href="/buy" class="nav-link">Покупка</a></li>
                    <li class="nav-item menu_item"><a href="/contacts" class="nav-link">Контакти</a></li>
                </ul>
            </div>
        </nav>
    </div>