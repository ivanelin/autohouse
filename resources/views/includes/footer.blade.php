<div class="container">
    <div class="row row-nav">
        <div class="col-4 offset-1">
            <img src="{{ asset('img/footer-LOGO.png') }}" alt="logo">
        </div>

        <div class="col-2 offset-1">
            <h5>Навігация</h5>
            <ul class="footer-nav">
                <li class="selected"><a href="/">Головна</a></li>
                <li><a href="/auto_in">Авто в ноявності</a></li>
                <li><a href="/buy">Покупка</a></li>
                <li><a href="/contacts">Контакти</a></li>
            </ul>
        </div>

        <div class="col-3">
            <h5>Контакти</h5>
            <ul class="footer-nav">
                <li>+38 (096) 090 77 00</li>
                <li><a href="#">OFFICE@AUTOHOUSE.LVIV.UA</a></li>
                <li><a href="#">FACEBOOK</a></li>
                <li><a href="#">INSTAGRAM</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-12 copyright">© 2018 BlaBlaDay</div>
    </div>
</div>