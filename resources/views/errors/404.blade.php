@extends('layouts.app')

@section('content')
<section class="bg-404">
    <div class="container">
        <div class="row">
            <div class="error-text col-5 ml-auto d-flex ">
                <h1>404</h1>
                <h2>Not Found</h2>
                <div class="error-msg">Сторінку по цьому адресу не знайдено. Можливо вона була видалена або перенесена.</div>
                <a href="/" class="button-main">На Головну</a>
            </div>
        </div>
    </div>
</section>
@endsection