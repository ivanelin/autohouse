<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}"> 
        <link rel="stylesheet" href="{{asset('css/custom.css')}}"> 
        <title> {{config('app.name', 'Autohouse')}} </title>

    </head>
        <body>
            {{-- HEADER --}}
            <header>
                @include('includes.header')
            </header>

            {{-- MAIN CONTENT --}}
                @yield('content')
        
            {{-- FOOTER --}}
            <footer>
                @include('includes.footer')
            </footer>
            

            <script src="{{asset('js/app.js')}}"></script>
            <script src="{{asset('js/custom.js')}}"></script>
          </body>

    
</html>
