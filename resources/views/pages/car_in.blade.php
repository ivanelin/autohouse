@extends('layouts.app')

@section('content')
   <!-- SECTION first-screen -->
<section class="first-screen first-screen_sub">
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="main_box main_box-sub">
                    <h1 class="h1-sub">Авто в ноявності</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="path">
    Головна → <span>Авто в наявності</span>
</div>

<!--Car rows-->
<section class="last_result">
    <div class="container">

        <div class="row car-row">
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src=" {{ asset('img/car-1.jpg') }} " alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Mercedes-Benz GLE-Class 2018</div>
                        <div class="car_info-price">51 962 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-2.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Audi Q7 3.0 TDI S-LINE 2018</div>
                        <div class="car_info-price">87 898 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-3.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Land Rover Range Rover Sport 2018</div>
                        <div class="car_info-price">78 132 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-4.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Bentley Bentayga Diesel 2018</div>
                        <div class="car_info-price">215 000 €</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row car-row">
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-1.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Mercedes-Benz GLE-Class 2018</div>
                        <div class="car_info-price">51 962 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-2.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Audi Q7 3.0 TDI S-LINE 2018</div>
                        <div class="car_info-price">87 898 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-3.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Land Rover Range Rover Sport 2018</div>
                        <div class="car_info-price">78 132 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-4.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Bentley Bentayga Diesel 2018</div>
                        <div class="car_info-price">215 000 €</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row car-row">
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-1.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Mercedes-Benz GLE-Class 2018</div>
                        <div class="car_info-price">51 962 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-2.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Audi Q7 3.0 TDI S-LINE 2018</div>
                        <div class="car_info-price">87 898 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-3.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Land Rover Range Rover Sport 2018</div>
                        <div class="car_info-price">78 132 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-4.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Bentley Bentayga Diesel 2018</div>
                        <div class="car_info-price">215 000 €</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a href="#" class="button button-result">Переглянути ще</a>
            </div>
        </div>
    </div>
</section> 

@include('includes.cta_form')
@endsection
