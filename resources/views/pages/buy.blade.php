@extends('layouts.app')

@section('content')
    
<!-- SECTION first-screen -->
<section class="first-screen first-screen_sub">
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="main_box main_box-sub">
                    <h1 class="h1-sub">Покупка</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<!--PATH-->
<div class="path">
    Головна → <span>Покупка</span>
</div>
<!--SOME WORDS-->
<section class="buy-words">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="buy-words-p">Ви бажаєте придбати сучасний автомобіль, але новий Вам не по кишені?</p>
                <p class="buy-words-p last">«Автохаус Львів» допоможе вирішити цю проблему. Ми радимо Вам звернути увагу на вживані автомобілі з пробігом, імпортного виробництва.</p>
            </div>
        </div>
    </div>
</section>
<!--CONSULTATION-->
<section class="consultation">
<div class="container">
    <div class="row">
        <div class="col-12">
            <h3>Консультація спеціалістів компанії</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p class="buy-words-p">Якщо Ви бажаєте довідатись актуальні дані про автомобіль, точну характеристику та параметри, досвідчені консультанти компанії допоможуть вибрати авто згідно Ваших вимог.</p>
            <p class="buy-words-p last">Зв’яжіться з професіоналами онлайн та отримайте відповіді на усі запитання. Ми будемо раді Вам допомогти!</p>
        </div>
    </div>
</div>
</section>

<!--CAR SELL-->
<section class="car-sell">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Процедура продажу автомобілів</h3>
            </div>
        </div>
        <div class="row">
                <div class="col-12">
                    <p class="buy-words-p">На сайті «Автохаус Львів» є можливість детально ознайомитись із характеристиками транспортного засобу, що Вас зацікавив. Прогляньте фото автомобілів та комплектації до них, а також інформацію про рік випуску, пробіг та технічні особливості. Ви знайдете свіжі новини та відгуки наших клієнтів, що допоможе швидко зробити правильний вибір та придбати авто для себе.</p>
                    <p class="buy-words-p last">Усі вживані автомобілі нашого салону юридично захищені відповідними документами. Кожне авто у чудовому технічному стані та з прозорою історією експлуатації.</p>
                </div>
            </div>
        </div>

    </div>
</section>

<!--BG GARANT-->
<section class="bg-garant">
    <div class="container">
        <div class="row">
            <div class="col-5">
                <div class="main_box main_box-sell">
                    Ми гарантуємо вигідну купівлю автомобіля у чудовому технічному стані та відмінного зовнішнього вигляду.Вигідна ціна, що відповідає стану авто та жодних проблем з оформленням документів. Наша ціль – стати Вашим впевненим вибором!
                </div>
            </div>
        </div>
    </div>
</section>

<!--INSURANCE-->
<section class="insurance">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Страхування та сервісне обслуговання</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="buy-words-p">Ми співпрацюємо із страховою компанією, що допоможе Вам оформити страхування на придбаний у «Автохаус Львів» автомобіль. При покупці автомобіля Ви гарантовано отримаєте знижку на страхування.</p>
                <p class="buy-words-p">Вам не варто хвилюватись на жодному етапі купівлі, адже професіонали автосалону на високому рівні готові надати Вам широкий спектр наступних послуг: продаж вживаних автомобілів, підготовка авто перед продажем, оформлення необхідних реєстраційних документів.</p>
                <p class="buy-words-p">«Автохаус Львів» відповідальний перед своїми покупцями, тому ми виконуємо роботу оперативно та ефективно. Якісний сервіс – це запорука надійного порозуміння з клієнтом.</p>
                <p class="buy-words-p last">З нами легко підібрати потрібний автомобіль. Вдалих Вам покупок!</p>
            </div>
        </div>
    </div>
</section>

@include('includes.cta_form')
@endsection