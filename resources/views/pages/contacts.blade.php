@extends('layouts.app')

@section('content')
    
<!-- SECTION first-screen -->
<section class="first-screen first-screen_sub">
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="main_box main_box-sub">
                    <h1 class="h1-sub">Контакти</h1>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="path">
    Головна → <span>Контакти</span>
</div>
<!--CONTACTS-->
<section class="contacts">
    <div class="contact-container">
        <ul class="contact-list">
            <li class="first-line">Телефон</li>
            <li>+38 (096) 090 77 00</li>
        </ul>
        <ul class="contact-list">
            <li class="first-line">Адреса</li>
            <li>Львівська область</li>
            <li>с.Сокільники</li>
            <li>вул.Скнилівська 23</li>
        </ul>
        <ul class="contact-list">
            <li class="first-line">Email</li>
            <li>office@autohouse.lviv.ua</li>
        </ul>
        <ul class="contact-list">
            <li class="first-line">Соціальні мережі</li>
            <li><a href="#">Facebook</a></li>
            <li><a href="#">Instagram</a></li>
        </ul>

    </div>
</section>

<!--MAP-->
<section class="map">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 map-container">

            </div>
        </div>
    </div>
</section>

@include('includes.cta_form')
@endsection