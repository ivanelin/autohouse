@extends('layouts.app')

@section('content')
<section class="first-screen">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="main_box">
                    <h1>&laquo;Автохаус львів&raquo;</h1>
                    <h2 class="main_box-slogan">Ми готові зробити Ваше життя простішим!</h2>
                    <p class="main_box-p">
                        Відшукайте натхнення та подаруйте собі комфорт і незабутні відчуття під час керування
                        автомобілем. «Автохаус Львів» – автосалон нового формату, де на першому місці виключно турбота
                        про клієнтів та гарантія якості.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- SECTION last_result -->
<section class="last_result">
    <div class="container">

        <div class="row">
            <div class="col-12">
                <h3>Останні находження</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-1.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Mercedes-Benz GLE-Class 2018</div>
                        <div class="car_info-price">51 962 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-2.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Audi Q7 3.0 TDI S-LINE 2018</div>
                        <div class="car_info-price">87 898 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-3.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Land Rover Range Rover Sport 2018</div>
                        <div class="car_info-price">78 132 €</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="car-item">
                    <div class="car-img">
                        <img src="{{ asset('img/car-4.jpg') }}" alt="car">
                    </div>
                    <div class="car_info">
                        <div class="car_info-name">Bentley Bentayga Diesel 2018</div>
                        <div class="car_info-price">215 000 €</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col"  >
                <a href="/car_in" class="button button-result">Переглянути ще</a>
            </div>
        </div>
    </div>
</section>

<!-- SECTION about_us -->
<section class="about_us">
    <div class="container fluid">
        <div class="row">
            <div class="col-12">
                <h3>Про нас</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 col-md-12">
                <div class="about_us-text">
                    <p>«Автохаус Львів» пропонує широкий спектр послуг з приводу купівлі, оформлення та подальшого
                        обслуговування вживаних автомобілів. Ексклюзивні моделі авто і чітко вказані рік та пробіг – це
                        наша перевага задля Вашої довіри. Асортимент послуг компанії:</p>
                    <ul class="about_us-ul">
                        <li>Продаж вживаних автомобілів відомих марок з Європи та північної Америки;</li>
                        <li>Викуп Ваших автомобілів;</li>
                        <li>Обмін на будь-яке авто нашого салону, що Вам до вподоби з умовою доплати з обох сторін;</li>
                        <li>Доступні кредитні пропозиції;</li>
                        <li>Автострахування;</li>
                        <li>Постановка і зняття автомобіля з обліку;</li>
                        <li>Допомога під час експлуатації придбаного автомобіля;</li>
                        <li>Встановлення додаткового обладнання;</li>
                        <li>Професійне підготування автомобіля до продажу.</li>
                    </ul>
                    <p></p>
                    <p>Якщо придбаєте автомобіль у нашому салоні, Ви отримаєте прозору сервісну історію, підтверджений
                        пробіг, безаварійний стан автомобіля, справедливу і привабливу ціну та абсолютну безпеку
                        покупки.</p>
                </div>
            </div>
            <div class="col-lg-5 col-md-12">
                <div class="about_us-img"></div>
            </div>
        </div>
    </div>
</section>

<!-- SECTION what_we_do -->
<section class="what_we_do">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3>Чим займається &laquo;Автохаус&raquo; Україна?</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Наша компанія з великим задоволенням робить життя людей комфортнішим, а купівлю автомобілів приємною
                    та безпечною.</p>
                <br>
                <p>Ми пропонуємо клієнтам широкий вибір сучасних автомобілів, а всі наші рішення базуються на інтересах
                    покупців.
                    Ви досягнете успіху та динамічності, відчуєте покращення якості життя та залишитесь задоволені своїм
                    вибором!</p>
            </div>
        </div>
    </div>
</section>
{{-- Include cta-form --}}
@include('includes.cta_form')
@endsection

