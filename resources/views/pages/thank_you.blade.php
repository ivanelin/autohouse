@extends('layouts.app')

@section('content')
<section class="thank-screen">
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="thank-screen-text">
                    <h1 class="h1-sub ">Дякуємо за заявку</h1>
                    <div class="row">
                        <div class="col-10 offset-1">
                            <p>Ваша заявка прийнята. Скоро наш менеджер з Вами звяжеться для вирішення всіх питаннь, які Вас цікавлять.</p>
                            <a href="/" class="button-main">На Головну</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
    
@endsection